function createUser() {
    let firstName, lastName;
    const newUser = {};

    do {
        firstName = prompt('Enter your name (english letters only)');
        lastName = prompt('Enter your surname (english letters only)');
    } while (validateValue(firstName) || validateValue(lastName));

    Object.defineProperties(newUser, {
        'firstName': {
            get() {
                return this._firstName;
            },

            set(value) {
                this._firstName = value;
            }
        },
        'lastName': {
            get() {
                return this._lastName;
            },

            set(value) {
                this._lastName = value;
            }
        }
    });

    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.getLogin = function() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }

    return newUser;
}

function validateValue(str) {
    return !str || str.match(/[^a-z]/gi);
}

const user = createUser();

console.log(user.getLogin());